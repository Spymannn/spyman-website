import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


import { HeaderComponent } from './header.component';

import { MatMenuModule, MatToolbarModule } from '@angular/material';

@NgModule({
	imports: [
		CommonModule,
		MatMenuModule,
		RouterModule,
		MatToolbarModule
	],
	exports: [
		MatMenuModule,
		MatToolbarModule,
		HeaderComponent
	],
	declarations: [
		HeaderComponent
	]
})
export class HeaderModule { }
