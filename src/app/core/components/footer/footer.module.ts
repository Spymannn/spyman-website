import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material';

import { FooterComponent } from './footer.component';


@NgModule({
	imports: [
        CommonModule,
        MatIconModule
    ],
	exports: [
        FooterComponent,
        MatIconModule
	],
	declarations: [
        FooterComponent
    ]
})
export class FooterModule { }
