import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameAWeekModule } from './game-a-week/game-a-week.module';
import { OtherGamesModule } from './other-games/other-games.module';


@NgModule({
	imports: [
		CommonModule,
        GameAWeekModule,
        OtherGamesModule
	],
	exports: [
        GameAWeekModule,
        OtherGamesModule
	],
	declarations: []
})
export class GamesModule { }
