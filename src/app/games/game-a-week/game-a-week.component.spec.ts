import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameAWeekComponent } from './game-a-week.component';

describe('GameAWeekComponent', () => {
  let component: GameAWeekComponent;
  let fixture: ComponentFixture<GameAWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameAWeekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameAWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
