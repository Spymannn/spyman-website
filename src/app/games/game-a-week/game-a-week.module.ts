import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameAWeekComponent } from './game-a-week.component';


@NgModule({
	imports: [
        CommonModule	
    ],
	exports: [
        GameAWeekComponent
	],
	declarations: [
        GameAWeekComponent
    ]
})
export class GameAWeekModule { }
