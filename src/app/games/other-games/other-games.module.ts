import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OtherGamesComponent } from './other-games.component';


@NgModule({
	imports: [
        CommonModule	
    ],
	exports: [
        OtherGamesComponent
	],
	declarations: [
        OtherGamesComponent
    ]
})
export class OtherGamesModule { }
