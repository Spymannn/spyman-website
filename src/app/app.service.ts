// ng dependencies
import { Injectable } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';

// npm dependencies

// service

// models

@Injectable()
export class AppService {

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute
	) { }
	initCookie() {
		// if (this.localStorageService.retrieve('firstLaunch') !== false) {
		// 	/*this.commonModal.open('Intro', 'intro', false);*/
		// 	// this.toastsManager
		// 	// 	.info('En continuant à naviguer sur ce site vous acceptez l\'utilisation de cookies afin d\'améliorer' +
		// 	// 	' votre navigation.Cliquez pour masquer ce message.',
		// 	// 	'Cookies',
		// 	// 	{ dismiss: 'click' });
		// 	// this.toastsManager.onClickToast().subscribe(toast => {
		// 	// });
		// 	this.localStorageService.store('firstLaunch', false);
		// }
	}
	initRouter() {
		this.router.events.subscribe(event => {
			window.scrollTo(0, 0);
		});
	}
}
