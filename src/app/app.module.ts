import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, provideRoutes } from '@angular/router';
import { HttpModule } from '@angular/http';

// angular material
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

// local dep
import { routing } from './app.routing';

// external modules
import { ModalModule } from 'angular-custom-modal';

// service
import { AppService } from './app.service';

import { AppComponent } from './app.component';

import { NotFoundComponent } from './not-found/not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';

// module made
import { CoreModule } from './core/core.module';
import { GamesModule } from './games/games.module';
import { GraphicsModule } from './graphics/graphics.module';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpModule,
    routing,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    CoreModule,
    GamesModule,
    GraphicsModule,
    ModalModule
  ],
  exports: [
    RouterModule,
    CoreModule,
    GamesModule,
    GraphicsModule,
    ModalModule
  ],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
