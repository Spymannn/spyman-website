import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pixel',
  templateUrl: './pixel.component.html',
  styleUrls: ['./pixel.component.css']
})
export class PixelComponent implements OnInit {
  imgNameModel: string = '';
  titleNameModel: string = '';

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 375);
  }

  setImage(imgName: string, titleName: string) {
    this.imgNameModel = imgName;
    this.titleNameModel = titleName;
  }

}
