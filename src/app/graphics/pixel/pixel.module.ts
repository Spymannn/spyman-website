import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PixelComponent } from './pixel.component';

import { ModalModule } from 'angular-custom-modal';


@NgModule({
  imports: [
      CommonModule,
      ModalModule
  ],
  exports: [
    PixelComponent
  ],
  declarations: [
    PixelComponent
  ]
})
export class PixelModule { }
