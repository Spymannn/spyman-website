import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IllustratorModule } from './illustrator/illustrator.module';
import { PixelModule } from './pixel/pixel.module';

@NgModule({
  imports: [
    CommonModule,
    IllustratorModule,
    PixelModule
  ],
  exports: [
    IllustratorModule,
    PixelModule
  ],
  declarations: []
})
export class GraphicsModule { }
