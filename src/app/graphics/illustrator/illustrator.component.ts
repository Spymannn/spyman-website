import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-illustrator',
  templateUrl: './illustrator.component.html',
  styleUrls: ['./illustrator.component.css']
})
export class IllustratorComponent implements OnInit {
  imgNameModel: string = '';
  titleNameModel: string = '';

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 375);
  }

  setImage(imgName: string, titleName: string) {
    this.imgNameModel = imgName;
    this.titleNameModel = titleName;
  }

}
