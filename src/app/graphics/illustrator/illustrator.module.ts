import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IllustratorComponent } from './illustrator.component';

import { ModalModule } from 'angular-custom-modal';


@NgModule({
  imports: [
      CommonModule,
      ModalModule
  ],
  exports: [
    IllustratorComponent,
    ModalModule
  ],
  declarations: [
    IllustratorComponent
  ]
})
export class IllustratorModule { }
