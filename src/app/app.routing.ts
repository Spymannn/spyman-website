// ng dependencies
import { RouterModule, Routes } from '@angular/router';

// custom services

// custom components
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { GameAWeekComponent, OtherGamesComponent } from './games/index';
import { PixelComponent, IllustratorComponent} from './graphics/index';

const routes: Routes = [
	{ path: 'home', component: HomeComponent },
	{ path: 'game-a-week', component: GameAWeekComponent },
	{ path: 'other-games', component: OtherGamesComponent },
	{ path: 'pixel', component: PixelComponent },
	{ path: 'illustrator', component: IllustratorComponent },
	{ path: '404', component: NotFoundComponent},
	{ path: '**', redirectTo: '/home' },
];

export const routing = RouterModule.forRoot(routes);
